# README #

This application is a test application for company CodeCacao. It is a REST-ful web service created with a SpringBoot.
Application is used for creating, retrieving, updating and deleting user. For database I used MySQL and IntelliJ as an IDE.
For testing application I used Postman.


### Starting application ###
These are the steps to start the application:

1. Clone or download application 
2. From command prompt navigate to application and run mvnw package ,this will create target file and jar
3. Crate a new schema in MySQL database called users_test(useraname: root, password:root - configuration of db is in application.properties file)
4. After creating schema application can be started with following command from command line: 
   <path to application location> java -jar target\users_test-0.0.1-SNAPSHOT.jar
5. Application is started


### How to use application ###

- I used Postman for testing application.
- First we need to register/authenticate:
	1. Open Postman, enter following url http://localhost:8080/login ; method: POST; body: {"username":"admin","password":"password"}; click SEND
	2. In response header you can see Authorization and value starts with Bearer 
	3. Now we have authorization value and we will pass it in header for other api calls
	3. Copy whole value (with Bearer) 
	
-Users options:
	1. Open new tab and in header insert key: Authorization value: paste the value from previous step
	2. Enter url http://localhost:8080/users ;
	3. method: OPTIONS;
	4. In response you will get available options for this url 
	
**For following steps you can use tab from previous step(then you don't have to insert Authorization header again) or you can open new tab (and add Authorization header) 
	
- Create user:
	1. Enter url http://localhost:8080/users ;
	2. method: POST;
	3. body:
		{
		  "title": "Mr.",
		  "firstName": "John",
		  "lastName": "Doe",
		  "email": "john.doe@i-ways.hr",
		  "phoneCode": "+385",
		  "phoneNumber": "003859126565484",
		  "password": "temp213test"
		}
	4. Click SEND	
	5. Now you have created User, and in response body you can see user details(or you can look in db)
	
-Update user: 
	1. Enter url http://localhost:8080/users/{user id} (if creating user went well you can insert number 1 as user id)
	2. method: PATCH
	3. body:
		{
		  "title": "Mr.",
		  "firstName": "John",
		  "lastName": "Doe",
		  "email": "john.doe@i-ways.hr",
		  "phoneCode": "+385",
		  "phoneNumber": "003859126565484",
		  "password": "1111"
		}
	4. Click SEND
	5. In response you can see that password for user John Doe is changed
	
-Delete user
	1. Enter url http://localhost:8080/users/{user id}
	2. method: DELETE
	3. Click SEND
	4. In response you can see that user is deleted

	
### REMARK ###
I have implemented JWT session handling but due to a lack of time I did not implement refresh token method. I set token duration for one day but it can be
changed in helpers/Constants class to check that it is working fine.