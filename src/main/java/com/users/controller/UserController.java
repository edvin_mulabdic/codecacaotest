package com.users.controller;

import com.users.helpers.Messages;
import com.users.model.User;
import com.users.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by edvin.mulabdic
 */
@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    UserService userService;

    public static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public void authenticate() {
    }

    @RequestMapping(value = "", method = RequestMethod.OPTIONS)
    public String optionsUserCollection() {
       return Messages.USER_COLLECTION_OPTIONS_MESSAGE;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public User registerUser(@RequestBody User user) {
       return userService.registerUserAccount(user);
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.OPTIONS)
    public String optionsUserAccount(@PathVariable("userId") Long id) {
       return userService.doesUserExist(id) ? Messages.USER_ACCOUNT_OPTIONS_MESSAGE : Messages.USER_NOT_FOUND;
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.PATCH)
    public User updateUserAccount(@PathVariable("userId") Long id, @RequestBody User user) {
       return userService.updateUser(id, user);
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public String getUserAccountDetails(@PathVariable("userId") Long id) {
       try {
           return userService.getUserDetails(id).toString();
       } catch (NullPointerException ex) {
           logger.error(ex.getLocalizedMessage());
           return Messages.USER_NOT_FOUND;
       }
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
    public String deleteUserAccount(@PathVariable("userId") Long id) {
       return userService.deleteUserAccount(id) ? Messages.USER_ACCOUNT_DELETED : Messages.USER_NOT_FOUND;
    }

}
