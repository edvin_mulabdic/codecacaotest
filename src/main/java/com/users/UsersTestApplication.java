package com.users;

import com.users.service.UserService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

/**
 * Created by edvin.mulabdic
 */

@SpringBootApplication
public class UsersTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsersTestApplication.class, args);
	}

    @Bean
    public UserService userService() {
        return new UserService();
    }

    @Bean
    public HibernateJpaSessionFactoryBean sessionFactory() {
        return new HibernateJpaSessionFactoryBean();
    }


}
