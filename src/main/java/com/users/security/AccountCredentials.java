package com.users.security;

/**
 * Created by edvin.mulabdic
 */

/*A class that is created for JWT Authenticator. This is just an example
* of a class, we can use our User model for authenticating  too
*/
public class AccountCredentials {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}