package com.users.dao;

import com.users.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by edvin.mulabdic
 */
@RepositoryRestResource
public interface UserDao extends CrudRepository<User, Long> {

}
