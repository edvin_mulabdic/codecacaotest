package com.users.helpers;

/**
 * Created by edvin.mulabdic
 */

public class Messages {
    /*Return messages*/
    public static final String USER_COLLECTION_OPTIONS_MESSAGE = "Allow: OPTIONS, GET, POST";
    public static final String USER_ACCOUNT_OPTIONS_MESSAGE = "Allow: OPTIONS, GET, PATCH";
    public static final String USER_NOT_FOUND = "User not found";
    public static final String USER_ACCOUNT_DELETED = "User account deleted";
}
