package com.users.helpers;

/**
 * Created by edvin.mulabdic
 */
public class Constants {
    /*Constants for JWT*/
    public static final long EXPIRATION_TIME = 86_400_000 ; // 1 day
    public static final String SECRET = "ThisIsASecret";
    public static final String TOKEN_PREFIX = "Bearer";
    public static final String HEADER_STRING = "Authorization";
    public static final String REFRESH_TOKEN = "jwt-refresh-token";
}
