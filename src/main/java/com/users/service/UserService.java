package com.users.service;

import com.users.dao.UserDao;
import com.users.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.constraints.Null;

/**
 * Created by edvin.mulabdic
 */
public class UserService  {
    public static final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    UserDao userDao;

    /*Creating user account*/
    public User registerUserAccount(User user) {
        try {
            return userDao.save(user);
        } catch (NullPointerException ex) {
            logger.error(ex.getMessage());
            return null;
        }
    }

    /*Updating user account*/
    public User updateUser(Long id, User user) {
        try {
            User updatedUser = userDao.findOne(id);
            updatedUser.setEmail(user.getEmail());
            updatedUser.setFirstName(user.getFirstName());
            updatedUser.setLastName(user.getLastName());
            updatedUser.setPassword(user.getPassword());
            updatedUser.setPhoneCode(user.getPhoneCode());
            updatedUser.setPhoneNumber(user.getPhoneNumber());
            updatedUser.setTitle(user.getTitle());
            userDao.save(updatedUser);
            userDao.save(updatedUser);
            return updatedUser;
        } catch (NullPointerException ex) {
            logger.error(ex.getMessage());
            return null;
        }
    }

    /*Deleting user account*/
    public Boolean deleteUserAccount(Long id) {
        try {
            userDao.delete(id);
            return true;
        } catch (NullPointerException ex) {
            logger.error(ex.getLocalizedMessage());
            return false;
        }
    }

    /*Getting user account details*/
    public User getUserDetails(Long id) {
        try {
            return userDao.findOne(id);
        } catch (NullPointerException ex) {
            logger.error(ex.getLocalizedMessage());
            return null;
        }
    }

    /*Checking if user account exist*/
    public Boolean doesUserExist(Long id) {
        return userDao.findOne(id) != null ? true : false;
    }


}
